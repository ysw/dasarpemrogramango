package common

type Request struct {
	Province string `json:"provID"`
	Url      string `json:"url"`
}

type Response struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}

type TravelList struct {
	Data []struct {
		AirlineName      string `json:"AirlineName,omitempty"`
		AirportName      string `json:"AirportName,omitempty"`
		CityName         string `json:"CityName,omitempty"`
		Currency         string `json:"Currency,omitempty"`
		DepartureDate    string `json:"DepartureDate,omitempty"`
		Description      string `json:"Description,omitempty"`
		Destination      string `json:"Destination,omitempty"`
		DetailTransit    string `json:"DetailTransit,omitempty"`
		DoubleType       string `json:"DoubleType,omitempty"`
		Duration         string `json:"Duration,omitempty"`
		Goods            string `json:"Goods,omitempty"`
		HotelName        string `json:"HotelName,omitempty"`
		HotelRating      string `json:"HotelRating"`
		Lat              string `json:"Lat"`
		LicenseNumber    string `json:"LicenseNumber"`
		Logo             string `json:"Logo"`
		Long             string `json:"Long"`
		Origin           string `json:"Origin"`
		OriginCity       string `json:"OriginCity"`
		Price            string `json:"Price"`
		PromoCode        string `json:"PromoCode"`
		PromoDescription string `json:"PromoDescription"`
		Provinsi         string `json:"Provinsi"`
		QuadType         string `json:"QuadType"`
		Rating           string `json:"Rating"`
		ReturnDate       string `json:"ReturnDate,omitempty"`
		TermCondition    string `json:"TermCondition,omitempty"`
		Transit          string `json:"Transit,omitempty"`
		TravelID         string `json:"TravelID"`
		TravelName       string `json:"TravelName"`
		TripID           string `json:"TripID"`
		TripleType       string `json:"TripleType,omitempty"`
	} `json:"data"`
	Message string `json:"message"`
	Status  string `json:"status"`
}

type UserTravel struct {
	Data struct {
		Avatar    string `json:"avatar"`
		Email     string `json:"email"`
		FirstName string `json:"first_name"`
		ID        int64  `json:"id"`
		LastName  string `json:"last_name"`
	} `json:"data"`
	Support struct {
		Text string `json:"text"`
		URL  string `json:"url"`
	} `json:"support"`
}

type RequestUser struct {
	UserID int `json:"userID"`
}

type ResponseUser struct {
	UserID   int    `json:"userID"`
	UserName string `json:"username"`
	Email    string `json:"email"`
}