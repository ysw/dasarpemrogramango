package controllers

import (
	"net/http"
	"echo-rest/common"
	"echo-rest/helpers"
	"echo-rest/handlers"

	"github.com/labstack/echo/v4"
)

//GeneratePassword ...
func GeneratePassword(c echo.Context) (err error) {

	req := new(common.Users)
	if err = c.Bind(req); err != nil {
		return err
	}

	hash, err := helpers.HashPassword(req.Password)

	return c.JSON(http.StatusOK, hash)
}

func CheckLogin(c echo.Context) (err error) {

	result, err := handlers.CheckUser(c)

	return c.JSON(http.StatusOK, result)
}
