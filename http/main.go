package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)


type TravelList struct {
	Data []struct {
		AirlineName      string `json:"AirlineName"`
		AirportName      string `json:"AirportName"`
		CityName         string `json:"CityName"`
		Currency         string `json:"Currency"`
		DepartureDate    string `json:"DepartureDate"`
		Description      string `json:"Description"`
		Destination      string `json:"Destination"`
		DetailTransit    string `json:"DetailTransit"`
		DoubleType       string `json:"DoubleType"`
		Duration         string `json:"Duration"`
		Goods            string `json:"Goods"`
		HotelName        string `json:"HotelName"`
		HotelRating      string `json:"HotelRating"`
		Lat              string `json:"Lat"`
		LicenseNumber    string `json:"LicenseNumber"`
		Logo             string `json:"Logo"`
		Long             string `json:"Long"`
		Origin           string `json:"Origin"`
		OriginCity       string `json:"OriginCity"`
		Price            string `json:"Price"`
		PromoCode        string `json:"PromoCode"`
		PromoDescription string `json:"PromoDescription"`
		Provinsi         string `json:"Provinsi"`
		QuadType         string `json:"QuadType"`
		Rating           string `json:"Rating"`
		ReturnDate       string `json:"ReturnDate"`
		TermCondition    string `json:"TermCondition"`
		Transit          string `json:"Transit"`
		TravelID         string `json:"TravelID"`
		TravelName       string `json:"TravelName"`
		TripID           string `json:"TripID"`
		TripleType       string `json:"TripleType"`
	} `json:"data"`
	Message string `json:"message"`
	Status  string `json:"status"`
}

type Users struct {
	Data struct {
		Avatar    string `json:"avatar"`
		Email     string `json:"email"`
		FirstName string `json:"first_name"`
		ID        int64  `json:"id"`
		LastName  string `json:"last_name"`
	} `json:"data"`
	Support struct {
		Text string `json:"text"`
		URL  string `json:"url"`
	} `json:"support"`
}

func handlePost(w http.ResponseWriter, r *http.Request){
	 //Encode the data
	 postBody, _ := json.Marshal(map[string]string{
		"provinsi":  "31",	   
	 })

	 responseBody := bytes.NewBuffer(postBody)
	  //Leverage Go's HTTP Post function to make request
	 resp, errs := http.Post("https://baramijintegrasi.com/travel/GetTripsSample.php", "application/json", responseBody)
	  //Handle Error
	 if errs != nil {
		log.Fatalf("An Error Occured %v", errs)
	 }
	 defer resp.Body.Close()
	  //Read the response body
	 body, err := ioutil.ReadAll(resp.Body)

	 if err != nil {
		log.Fatalln(err)
	 }
	 
	 var TravelList TravelList

	 if errs = json.Unmarshal(body, &TravelList); err != nil {
		 log.Fatalln(errs)
	 }

	 //sb := string(body)
     //log.Printf(sb)

	 for _, s := range TravelList.Data {
		fmt.Println("Airline Name",s.AirlineName)
	 }	
 }

 func handleGet(w http.ResponseWriter, r *http.Request) {

	resp, err := http.Get("https://reqres.in/api/users/2")
	if err != nil {
	   log.Fatalln(err)
	}
 //We Read the response body on the line below.
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
	   log.Fatalln(err)
	}
 //Convert the body to type string
	var users Users

	if err = json.Unmarshal(body, &users); err != nil {
		log.Fatalln(err)
	}

	//sb := string(body)
    //log.Printf(sb)
	
	fmt.Println("First Name" , users.Data.FirstName)
 
}
 func main() { 	

	http.HandleFunc("/post", handlePost)
	http.HandleFunc("/get", handleGet)
    http.ListenAndServe(":8080", nil)
 
}